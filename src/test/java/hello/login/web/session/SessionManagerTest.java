package hello.login.web.session;

import hello.login.domain.member.Member;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import static org.assertj.core.api.Assertions.assertThat;

class SessionManagerTest {

    SessionManager sessionManager = new SessionManager();

    @Test
    void sessionTest() {

        // 서버가 세션 생성하고 client 에게 sessionId 를 쿠키에 담아서 응답
        MockHttpServletResponse response = new MockHttpServletResponse();
        Member member = new Member();
        sessionManager.createSession(member, response);

        // sessionId 받은 client 는 sessionId 를 쿠키에 담아서 요청
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setCookies(response.getCookies());

        // 서버는 client 의 요청 쿠키에 담긴 sessionId 로 세션 정보(result) 조회 및 검증
        Object result = sessionManager.getSession(request);
        assertThat(result).isEqualTo(member);

        // 서버가 client 의 세션 정보를 만료시키면 요청 쿠키에 담긴 sessionID 로 세션 정보 조회 불가
        sessionManager.expire(request);
        Object expired = sessionManager.getSession(request);
        assertThat(expired).isNull();

    }
}