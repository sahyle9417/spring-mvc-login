package hello.login.web.filter;

import hello.login.web.SessionConst;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.PatternMatchUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.UUID;

@Slf4j
public class LoginCheckFilter implements Filter {

    private static final String[] whitelist = {"/", "/members/add", "/login", "/logout", "/css/*"};

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        String requestURI = httpRequest.getRequestURI();
        String uuid = UUID.randomUUID().toString();
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        try {
            log.info("[{}] [{}] LoginCheckFilter start", requestURI, uuid);

            // 로그인 체크 대상 URI 인 경우
            if (isLoginCheckPath(requestURI)) {
                HttpSession session = httpRequest.getSession(false);
                if (session == null || session.getAttribute(SessionConst.LOGIN_MEMBER) == null) {
                    log.info("[{}] [{}] LoginCheckFilter check fail", requestURI, uuid);
                    // 로그인 페이지(/login)로 redirect 되도록 response 설정
                    // 로그인 성공 시 복귀할 수 있게 현재 URL 을 redirectURL 매개변수로 전달
                    httpResponse.sendRedirect("/login?redirectURL=" + requestURI);
                    // 메서드 종료 (이거 빼먹어서 잠깐 삽질함)
                    return;
                }
                log.info("[{}] [{}] LoginCheckFilter check success", requestURI, uuid);
            }

            // 로그인 체크 대상 URI 아니거나, 로그인 체크 성공
            // 아래와 같이 chain.doFilter 호출해야 다음 필터 또는 controller 가 정상 실행됨 (누락하면 요청 처리 안됨) (필터 단점)
            chain.doFilter(request, response);

        } catch (Exception e) {
            // 톰캣에게까지 예외 보내줘야 함
            throw e;
        } finally {
            log.info("[{}] [{}] LoginCheckFilter end", requestURI, uuid);
        }
    }

    // 로그인 체크 대상 URI 여부
    private boolean isLoginCheckPath(String requestURI) {
        // whitelist 에 포함되지 않는 URI 는 로그인 체크 대상 URI
        return !PatternMatchUtils.simpleMatch(whitelist, requestURI);
    }

}
