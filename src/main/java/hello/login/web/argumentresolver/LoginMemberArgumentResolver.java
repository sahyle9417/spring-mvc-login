package hello.login.web.argumentresolver;

import hello.login.domain.member.Member;
import hello.login.web.SessionConst;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Slf4j
public class LoginMemberArgumentResolver implements HandlerMethodArgumentResolver {

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        log.info("supportsParameter start");

        // parameter 가 Login 어노테이션을 가지고 있는지 여부
        boolean hasLoginAnnotation = parameter.hasParameterAnnotation(Login.class);

        // parameter 가 Member 타입을 할당받을 수 있는지 여부 (parameter 가 Member 타입이거나 Member 의 부모 타입인지 여부)
        boolean hasMemberType = Member.class.isAssignableFrom(parameter.getParameterType());

        // 위의 2가지 조건을 모두 충족해야 support 성립
        return hasLoginAnnotation && hasMemberType;
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        log.info("resolveArgument start");

        // request 의 세션 정보가 존재하면 세션 정보에 로그인 사용자(loginMember) 객체 반환
        HttpServletRequest request = (HttpServletRequest) webRequest.getNativeRequest();
        HttpSession session = request.getSession(false);
        if (session == null) {
            return null;
        }
        return session.getAttribute(SessionConst.LOGIN_MEMBER);
    }
}
