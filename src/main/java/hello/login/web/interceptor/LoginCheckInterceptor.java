package hello.login.web.interceptor;

import hello.login.web.SessionConst;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.UUID;

@Slf4j
public class LoginCheckInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String requestURI = request.getRequestURI();
        String uuid = UUID.randomUUID().toString();
        log.info("[{}] [{}] [{}] LoginCheckInterceptor preHandle", requestURI, uuid, handler);

        HttpSession session = request.getSession(false);

        // 세션 정보 없는 경우 로그인 페이지로 redirect 후 종료
        if (session == null || session.getAttribute(SessionConst.LOGIN_MEMBER) == null) {
            log.info("[{}] [{}] [{}] LoginCheckInterceptor preHandle check fail", requestURI, uuid, handler);
            response.sendRedirect("/login?redirectURL=" + requestURI);
            // 비정상 호출(로그인 없이 로그인 필요 페이지 접근)인 경우 false 반환하면 다음 interceptor 또는 controller 호출 X
            return false;
        }

        // 정상 호출인 경우 true 를 반환하며 이후 다음 interceptor 또는 controller 가 호출됨
        return true;
    }
}
