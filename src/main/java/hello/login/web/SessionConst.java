package hello.login.web;

// LOGIN_MEMBER 값을 사용하기 위한 것이므로 객체 생성하지 못하게 interface 로 지정
public interface SessionConst {
    String LOGIN_MEMBER = "loginMember";
}
