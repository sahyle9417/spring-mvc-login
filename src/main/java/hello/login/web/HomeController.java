package hello.login.web;

import hello.login.domain.member.Member;
import hello.login.domain.member.MemberRepository;
import hello.login.web.argumentresolver.Login;
import hello.login.web.session.SessionManager;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.SessionAttribute;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Slf4j
@Controller
@RequiredArgsConstructor
public class HomeController {

    private final MemberRepository memberRepository;
    private final SessionManager sessionManager;

    // @GetMapping("/")
    public String home() {
        return "/home";
    }

    // @GetMapping("/")
    public String homeLoginV1(@CookieValue(name = "memberId", required = false) Long memberId, Model model) {

        // memberId 가 null 이거나 유효하지 않다면 로그인 실패하고 기본 홈 화면으로 이동
        if (memberId == null) {
            return "/home";
        }
        Member loginMember = memberRepository.findById(memberId);
        if (loginMember == null) {
            return "/home";
        }

        // memberId 가 유효하다면 로그인 성공한 홈 화면으로 이동
        model.addAttribute("member", loginMember);
        return "/loginHome";
    }

    // @GetMapping("/")
    public String homeLoginV2(HttpServletRequest request, Model model) {

        // 세션 관리자에 저장된 회원 정보 조회
        Member loginMember = (Member) sessionManager.getSession(request);

        // memberId 가 null 이거나 유효하지 않다면 로그인 실패하고 기본 홈 화면으로 이동
        if (loginMember == null) {
            return "/home";
        }

        // memberId 가 유효하다면 로그인 성공한 홈 화면으로 이동
        model.addAttribute("member", loginMember);
        return "/loginHome";
    }

    // @GetMapping("/")
    public String homeLoginV3(HttpServletRequest request, Model model) {

        // 세션이 있으면 기존 세션 반환, 없으면 null 반환 (create = false)
        HttpSession session = request.getSession(false);
        if (session == null) {
            return "/home";
        }

        // 세션에 저장된 회원 정보 조회
        Member loginMember = (Member) session.getAttribute(SessionConst.LOGIN_MEMBER);

        // 세션에 회원 정보 없으면 로그인 실패하고 기본 홈 화면으로 이동
        if (loginMember == null) {
            return "/home";
        }

        // 세션에 회원 정보 있으면 로그인 성공한 홈 화면으로 이동
        model.addAttribute("member", loginMember);
        return "/loginHome";
    }

    // @GetMapping("/")
    public String homeLoginV4(
            @SessionAttribute(name = SessionConst.LOGIN_MEMBER, required = false) Member loginMember,
            Model model) {

        // 세션에 회원 정보 없으면 로그인 실패하고 기본 홈 화면으로 이동
        if (loginMember == null) {
            return "/home";
        }

        // 세션에 회원 정보 있으면 로그인 성공한 홈 화면으로 이동
        model.addAttribute("member", loginMember);
        return "/loginHome";
    }

    @GetMapping("/")
    public String homeLoginV5(@Login Member loginMember, Model model) {

        // 세션에 회원 정보 없으면 로그인 실패하고 기본 홈 화면으로 이동
        if (loginMember == null) {
            return "/home";
        }

        // 세션에 회원 정보 있으면 로그인 성공한 홈 화면으로 이동
        model.addAttribute("member", loginMember);
        return "/loginHome";
    }

}